from typing import Callable
import math
def linear_schedule(initial_value: float, minimum: float=0) -> Callable[[float], float]:
    """
    Linear scheduler

    :param initial_value: Initial learning rate.
    :param minimum: Minimum value, 0 by default.
    :return: schedule that computes
      current the next value depending on remaining progress
    """
    def func(progress_remaining: float) -> float:
        """
        Progress will decrease from 1 (beginning) to 0.

        :param progress_remaining:
        :return: current value computed from the progress_remaining
        """
        current = progress_remaining * initial_value #compute the current_value
        #return the value only if it is bigger than the minimum we set
        if current<minimum:
            return minimum
        else:
            return current
    return func

def multiplier_scheduler(initial_value: float, multiplier: float=0.5, minimum: float=0) -> Callable[[float], float]:
    """
    Scheduler that multiplies the initial_value by the multiplier each time the progress increases by 0.1.

    :param initial_value: Initial learning rate.
    :param multiplier: By how much to multiply the value.
    :param minimum: Minimum value, 0 by default.
    :return: schedule that computes
      current the next value depending on remaining progress and the multiplier
    """
    def func(progress_remaining: float) -> float:
        """
        Progress will decrease from 1 (beginning) to 0.

        :param progress_remaining:
        :return: current value computed from the progress_remaining
        """

        if progress_remaining>0.85:
            return initial_value
        else:
            return max((multiplier**((1-progress_remaining)//0.15))*initial_value,minimum)
    return func
