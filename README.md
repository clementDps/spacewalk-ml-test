# Spacewalk ML test

Full details on our tests are presented in the Spacewalk_ML_Test_Report.pdf file.

 Setting up the environment
 --
 In order to run the python codes, you can create a virtual environment with the given requirements.txt file by running:
 ```
python3 -m venv spacewalk_env
./spacewalk_env/Scripts/activate
pip install -r requirements.txt
pip install git+https://github.com/TeamSPWK/spwk-ml-test.git
 ```
This will install all the libraries we've used that are necessary to run the code.

Looking at the results
--
We included the results of our trainings in the folder ppo_tensorboard. You can look at them by running:
```
tensorboard --logdir ppo_tensorboard
```
Within each folder, (v1,v2...), we also included the best saved agent that you can test using the test_model.py file.

You can also visualize the performance of a model with a particular environment by using the test_choose_env.py file.

Files
--

train.py : file training our agent, run it to train a new agent

custom_model.py : specifies the feature extractor we used

gymWrapper.py : gym wrapper for the FillPolygon environment

test_model.py : test the specified model and prints its avg reward

test_choose_env.py : test the specified model with the specified env number and displays the result

linear_schedule.py : defines the functions used to decrease the learning rate/clip rate during training
