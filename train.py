from stable_baselines3.common.vec_env import VecTransposeImage, VecEnv
from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.callbacks import EvalCallback
from stable_baselines3.common.vec_env import VecTransposeImage, VecEnv
from stable_baselines3 import PPO
from typing import Dict, Any
import numpy as np
import cv2
from linear_schedule import linear_schedule, multiplier_scheduler
from custom_model import CustomCNN
from gymWrapper import GymWrapperEnv

# Callback to test the agent on eval_env and saves the best performing one
eval_env=VecTransposeImage(make_vec_env(GymWrapperEnv,n_envs=1))
eval_callback = EvalCallback(eval_env, best_model_save_path='./logs/',n_eval_episodes=50,
                             log_path='./logs/', eval_freq=10000,
                             deterministic=True, render=False)

# Create 6 environments in parallel
env = make_vec_env(GymWrapperEnv,n_envs=6)

# Specify the feature extractor as well as the architecture for the value function (vf) and the policy (pi)
policy_kwargs = dict(features_extractor_class=CustomCNN,
                     net_arch=[dict(vf=[64], pi=[64])])

# Create a PPO Agent on our environment
model = PPO("CnnPolicy", env, learning_rate=linear_schedule(0.0003,0.00005),
            clip_range=linear_schedule(0.2,0.05),policy_kwargs=policy_kwargs,batch_size=256,
            target_kl = 0.3, verbose=1,tensorboard_log="./ppo_tensorboard/")

# Displays the architecture of the policy
print(model.policy)

# Train the agent for 5M steps
model.learn(total_timesteps=5000000,callback=eval_callback)
