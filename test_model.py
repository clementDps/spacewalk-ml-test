from stable_baselines3 import PPO
from gymWrapper import GymWrapperEnv
import numpy as np
from stable_baselines3.common.env_util import make_vec_env
import cv2

#load a model
model = PPO.load(".\\ppo_tensorboard\\v6\\best_model.zip")

#create the testing environment
env = make_vec_env(GymWrapperEnv,n_envs=1)

#some variable to compute statistics later
i=0
step_counter=0
reward_sum = 0
max_episode=1000

obs = env.reset()
while i<max_episode:
    action, _states = model.predict(obs)
    obs, reward, done, info = env.step(action)
    reward_sum+=reward
    step_counter+=1
    if done:
        i+=1
    #Uncomment to see the agent in action
    """
    cv2.imshow('Environment', np.squeeze(obs))
    cv2.waitKey(0)
    """
print("avg reward is "+str(reward_sum/max_episode))
print("avg episode length is "+str(step_counter/max_episode)+" so we placed "+str((step_counter/max_episode)-1)+" parking slots on average")
