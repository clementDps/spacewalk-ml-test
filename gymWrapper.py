from spwkml import PolygonFillEnv
import numpy as np
from shapely import geometry
import cv2
import gym
from gym import Env, spaces
import math
import random

class GymWrapperEnv(gym.Env):
    """
    Gym wrapper for the PolygonFillEnv
    Fill polygon space with polygon patches
    """

    def __init__(self):
        self.img_res = 128
        self.angle_res = 36
        self.coord_mp = float(self.img_res / 20)
        self.angle_mp = float(self.angle_res / np.pi)
        self.original_env = PolygonFillEnv()
        self.patch_area = geometry.asPolygon(self.original_env.patch).area

        #required parameters for the gym environment to be usable by other libraries
        self.observation_space=spaces.Box(low = np.zeros([128,128,3]),
                                            high = np.full([128,128,3],255),
                                            dtype = np.uint8) # the observation space is an image with values 0 to 255
        self.action_space=spaces.Box(low=np.array([-1,-1,-1]),high=np.array([1,1,1]),dtype=np.float32) #the action space is normalized
        self.reset()

    def reset(self):
        self.original_env.select_space(random.randint(0,9)) #each time we reset, we also randomly change the environment
        self.original_env.reset()
        self.canvas = np.zeros((self.img_res,self.img_res,3), dtype=np.uint8)
        self.draw(self.original_env.space['shell'], (1,0,0))
        for hole in self.original_env.space['holes']:
            self.draw(hole, (0,0,0))
        return self.canvas.astype(np.bool).astype(np.float32)*255

    def select_space(self, idx):
        self.original_env.select_space(idx)
        self.reset()

    def draw(self, arr, color, to_canvas=True):
        pts = np.around((arr + 10) * self.coord_mp).astype(np.int32)
        img = cv2.fillPoly(np.zeros(self.canvas.shape, dtype=np.uint8), [pts], color)
        if to_canvas:
            cv2.fillPoly(self.canvas, [pts], color)
        return img


    def step(self, action):
        action_x, action_y, action_angle = np.squeeze(action)

        # Change the range of actions:
        action_x = ((action_x+1)/2)*128
        action_y = ((action_y+1)/2)*128
        action_angle = ((action_angle+1)/2)*36
        # xy range : 0 ~ 128 (1 bucket added)
        # angle range : 0 ~ 36 (1 bucket added)
        patch_x = float(action_x) / self.coord_mp - 10
        patch_y = float(action_y) / self.coord_mp - 10
        patch_angle = float(action_angle) / self.angle_mp - np.pi/2
        data = self.original_env.step(patch_x, patch_y, patch_angle)
        data['patch_raster'] = self.draw(data['selected_patch'], (0,1,0), to_canvas=data['is_valid']).astype(np.bool).astype(np.float32)
        data['state_raster'] = self.canvas.astype(np.bool).astype(np.float32)

        parking_score = data['n_patches'] / 32 # 0 ~ 1
        area_penalty = -(data['area_out_of_space'] + data['area_intersect_patches']) / self.patch_area # -1 ~ 0

        data['rew'] = parking_score + area_penalty
        data['done'] = not data['is_valid']

        if data['done']:
            self.reset()
        # Returns next state, reward, done, debugging info
        return (data['state_raster']*255,data['rew'],data['done'],dict())

    def render(self, save_img=False, path=None, fname=None, show_last=True, show_axis=True):
        self.original_env.render(save_img=save_img, path=path, fname=fname, show_last=show_last, show_axis=show_axis)

    """
    same to the reset function
    except we give a specific env_number
    """
    def reset_with_number(self, env_number):
        self.original_env.select_space(env_number) #we reset the env while selecting the one we want, used for analyzing results
        self.original_env.reset()
        self.canvas = np.zeros((self.img_res,self.img_res,3), dtype=np.uint8)
        self.draw(self.original_env.space['shell'], (1,0,0))
        for hole in self.original_env.space['holes']:
            self.draw(hole, (0,0,0))
        return self.canvas.astype(np.bool).astype(np.float32)*255


    @property
    def spaces(self):
        return self.original_env.spaces

    @property
    def n_steps(self):
        return self.original_env.n_steps

    @property
    def n_patches(self):
        return self.original_env.n_patches

    @property
    def placed_patches(self):
        return self.original_env.placed_patches

    @property
    def patch(self):
        return self.original_env.patch

    @property
    def space(self):
        return self.original_env.space

    @property
    def new_patch(self):
        return self.original_env.new_patch
