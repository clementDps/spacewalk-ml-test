from stable_baselines3 import PPO
from gymWrapper import GymWrapperEnv
import numpy as np
from stable_baselines3.common.env_util import make_vec_env
import cv2
import argparse


parser = argparse.ArgumentParser(description='Test a model with a given environment number')
parser.add_argument('--env', type=int, default=0,
                    help='number of the env, between 0 and 9')
args = parser.parse_args()
env_number = args.env
#load a model
model = PPO.load(".\\ppo_tensorboard\\v6\\best_model.zip")

#create the testing environment
env = GymWrapperEnv()


done = False

obs = env.reset_with_number(env_number)
cv2.imshow('Environment', np.squeeze(obs))
cv2.waitKey(0)
while not done:
    action, _states = model.predict(obs)
    obs, rewards, done, info = env.step(action)

    cv2.imshow('Environment', np.squeeze(obs))
    cv2.waitKey(0)
